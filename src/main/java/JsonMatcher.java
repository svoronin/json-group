import com.google.gson.Gson;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JsonMatcher {

    private static class RegionPair {
        String tag;
        String name;
    }

    private static class DistinctRegionPair {
        String tag;
        String[] names;

        public static DistinctRegionPair fromRegionalPairs(Collection<RegionPair> regionaPairs) {
            DistinctRegionPair newPair = new DistinctRegionPair();
            newPair.tag = regionaPairs.iterator().next().tag;
            newPair.names = regionaPairs.stream().map(pair -> pair.name).collect(Collectors.toList()).toArray(new String[0]);
            return newPair;
        }
    }

    public static void main(String[] args) throws IOException {
        StringBuilder json = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(isr);
        for(String line = reader.readLine(); line != null; line = reader.readLine()) {
            json.append(line);
        }

        Gson parser = new Gson();
        RegionPair[] sourcePairs = parser.fromJson(json.toString(), RegionPair[].class);

        Map<String, List<RegionPair>> groupedPairs = Arrays.asList(sourcePairs).stream().collect(Collectors.groupingBy(pair -> pair.tag));
        DistinctRegionPair[] distinctPairs = groupedPairs.entrySet().stream().map(pair -> DistinctRegionPair.fromRegionalPairs(pair.getValue())).collect(Collectors.toList()).toArray(new DistinctRegionPair[0]);
        System.out.println(parser.toJson(distinctPairs, DistinctRegionPair[].class));
    }
}